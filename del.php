<?php

use Predis\Client;

require_once('vendor/autoload.php');

require 'autoload.php';
require 'db.php';
require 'style.php';
  $redis=new Predis\Client();
require 'db.php';
$startsql = microtime(true);
$sql = 'SELECT * FROM utilisateurs ORDER BY note DESC';
$statement = $connection->prepare($sql);
$statement->execute();
$people = $statement->fetchAll(PDO::FETCH_OBJ);
$endsql=microtime(true)-$startsql;
$messagesql=$endsql;
 ?>
<?php require 'header.php'; ?>
<div class="container"> 
<a style="color: mistyrose;float: left;" href="index.php"><span class="fa fa-caret-left">&ensp;Insertion </span></a><a style="color: mistyrose;float: right;" href="union.php">union <span class="fa fa-caret-right"></span></a>

    <div class="row">
        <div class="col-md-7">
            <div class="card mt-5">
                <div class="card-header">
                <h2>Suppression Utilisateurs&ensp;<span class="fa fa-trash"></h2>
                </div>
                <div class="card-body">
                  <h5>Mysql</h5>
      <table class="table table-bordered">
        <tr>
          <th>Nom</th>
          <th>Note</th>
          <th>Action</th>
        </tr>
        <?php foreach($people as $person): ?>
          <tr>
            <td><?= $person->nom; ?></td>
            <td><?= $person->note; ?></td>
            <td>
              <a onclick="return confirm('Are you sure you want to delete this entry?')" href="delete.php?nom=<?= $person->nom ?>" class='btn btn-danger'>Delete</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>

    </div>
                </div>
            </div>
        <div class="col-md-5">
            <div class="card mt-5">
                    <div class="card-header">
                    <h2>Temps exécution&ensp;<span class="fa fa-clock-o"></h2>
                    </div>
                    <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Rédis</th>
                            <th>MySql</th>
                        </tr>
                        <tr>
                            <td>
                              <?php 
                              echo $_GET['messredis'];
                             ?></td>
                            <td>
                              <?php 
                               echo $_GET['messsql'];
                      
                            ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
 
</div>
<?php require 'footer.php'; ?>
