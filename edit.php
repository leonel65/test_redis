<?php
require 'db.php';
require 'style.php';
$nom = $_GET['nom'];
$sql = 'SELECT * FROM utilisateurs WHERE nom=:nom';
$statement = $connection->prepare($sql);
$statement->execute([':nom' => $nom ]);
$person = $statement->fetch(PDO::FETCH_OBJ);
if (isset ($_POST['nom'])  && isset($_POST['note']) ) {
  $noms = $_POST['nom'];
  $note = $_POST['note'];
  $startsql = microtime(true);
  $sql = 'UPDATE utilisateurs SET nom=:nom, note=:note WHERE nom=:nom';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':nom' => $noms, ':note' => $note])) {
  $messagesql=microtime(true)-$startsql;
    header("Location:supp.php");
  }



}


 ?>
<?php require 'header.php'; ?>
<div class="container">
  <div class="row">
    <div class="col-md-7">
        <div class="card mt-5">
        <div class="card-header">
          <h2>Update Users <span class="fa fa-pencil"></span></h2>
        </div>
        <div class="card-body">
          <?php if(!empty($message)): ?>
            <div class="alert alert-success">
              <?= $message; ?>
            </div>
          <?php endif; ?>
          <form method="post">
            <div class="form-group">
              <label for="name">Nom</label>
              <input value="<?= $person->nom; ?>" type="text" name="nom" id="nom" class="form-control">
            </div>
            <div class="form-group">
              <label for="email">note</label>
              <input type="number" value="<?= $person->note; ?>" name="note" id="note" class="form-control" min="0" max="20">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-info">Update person</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-5">
              <div class="card mt-5">
                      <div class="card-header">
                      <h2>Temps exécution&ensp;<span class="fa fa-clock-o"></h2>
                      </div>
                      <div class="card-body">
                      <table class="table table-bordered">
                          <tr>
                              <th>Rédis</th>
                              <th>MySql</th>
                          </tr>
                          <tr>
                              <td><?php echo 0?></td>
                              <td><?php echo 0?></td>
                          </tr>
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<?php require 'footer.php'; ?>
