<?php

use Predis\Client;

require_once('vendor/autoload.php');

require 'autoload.php';
require 'db.php';
require 'style.php';
$messageredis = 0;
$messagemysql = 0;

/*
$r="SELECT 
CONCAT(ROUND(((DATA_LENGTH + INDEX_LENGTH - DATA_FREE) / 1024 / 1024), 2), 'Mo') AS TailleMo 
FROM information_schema.TABLES 
WHERE TABLE_SCHEMA = 'company'
AND   TABLE_NAME = 'utilisateurs';";

$statements = $connection->prepare($r);
$statements->execute();
$peoples = $statements->fetchAll(PDO::FETCH_OBJ);

*/
  $redis=new Predis\Client();
if (isset ($_POST['name'])  && isset($_POST['note']) ) {
  $name = $_POST['name'];
  $note = $_POST['note'];
  $startMysql=microtime(true);
  $sql = 'INSERT INTO utilisateurs(nom, note) VALUES(:nom, :note)';
  $statement = $connection->prepare($sql);
  $statement->execute([':nom' => $name, ':note' => $note]) ;
  $messagemysql =  microtime(true)-$startMysql;
  

  $startredis=microtime(true);
  $redis->zadd('eleve',$note,$name);
  $messageredis =  microtime(true)-$startredis;


}


 ?>
<?php require 'header.php'; ?>

<div class="container">
<a style="color: mistyrose;float: right;" href="list.php">List utilisateur <span class="fa fa-caret-right"></span></a>
    <div class="row">
        <div class="col-md-7">
            <div class="card mt-5">
                <div class="card-header">
                <h2>Insertion Utilisateurs&ensp;<span class="fa fa-save"></h2>
                </div>
                    <form method="post">
                        <div class="form-group">
                        <label for="name">Nom</label>
                        <input type="text" name="name" id="name" class="form-control">
                        </div>
                        <div class="form-group">
                        <label for="email">Note</label>
                        <input type="number" name="note" min='00' max="20" id="note" class="form-control">
                        </div>
                        <div class="form-group"><br>
                        <button type="submit" style="font-weight: bold;" class="btn btn-info offset-5">Ajouter&ensp;<span class="fa fa-plus-circle"></button>
                        </div>
                    </form>
                </div>
            </div>
        <div class="col-md-5">
            <div class="card mt-5">
                    <div class="card-header">
                    <h2>Temps exécution&ensp;<span class="fa fa-clock-o"></h2>
                    </div>
                    <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Rédis</th>
                            <th>MySql</th>
                        </tr>
                        <tr>
                            <td><?php echo $messageredis?></td>
                            <td><?php echo $messagemysql?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php
       
    ?>
</div>
<?php require 'footer.php'; ?>
