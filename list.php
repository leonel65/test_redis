<?php

use Predis\Client;

require_once('vendor/autoload.php');

require 'autoload.php';
require 'db.php';
require 'style.php';
  $redis=new Predis\Client();
require 'db.php';
$startsql = microtime(true);
$sql = 'SELECT * FROM utilisateurs ORDER BY note ASC';
$statement = $connection->prepare($sql);
$statement->execute();
$people = $statement->fetchAll(PDO::FETCH_OBJ);
$endsql=microtime(true)-$startsql;
$messagesql=$endsql;
 ?>
<?php require 'header.php'; ?>
<div class="container">
<a style="color: mistyrose;float: left;" href="index.php"><span class="fa fa-caret-left">&ensp;Insertion </span></a><a style="color: mistyrose;float: right;" href="supp.php">supression <span class="fa fa-caret-right"></span></a>

    <div class="row">
        <div class="col-md-7">
            <div class="card mt-5">
                <div class="card-header">
                <h2>Liste Utilisateurs&ensp;<span class="fa fa-users"></h2>
                </div>
                <div class="card-body">
                  <h5>Mysql</h5>
      <table class="table table-bordered">
        <tr>
          <th>Nom</th>
          <th>Note</th>
        </tr>
        <?php foreach($people as $person): ?>
          <tr>
            <td><?= $person->nom; ?></td>
            <td><?= $person->note; ?></td>
          </tr>
        <?php endforeach; ?>
      </table>
<hr>
<h5>Redis</h5>
      <table class="table table-bordered">
        <tr>
          <th>Nom</th>
          <th>Note</th>
        </tr>
        <?php
        $startredis = microtime(true);
        $nom=$redis->zrange('eleve',0,-1);
        $note=$redis->zrange('eleve',0,-1,'withscores');
        $messageredis=microtime(true)-$startredis;
          foreach(array_combine($nom,$note) as $n=>$b): ?>
          <tr>
            <td><?= $n?></td>
            <td><?= $b; ?></td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
                </div>
  </div>
        <div class="col-md-5">
            <div class="card mt-5">
                    <div class="card-header">
                    <h2>Temps exécution&ensp;<span class="fa fa-clock-o"></h2>
                    </div>
                    <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Rédis</th>
                            <th>MySql</th>
                        </tr>
                        <tr>
                            <td><?php echo $messageredis?></td>
                            <td><?php echo $messagesql?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
 
</div>
<?php require 'footer.php'; ?>
