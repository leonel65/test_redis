<?php

use Predis\Client;

require_once('vendor/autoload.php');

require 'autoload.php';
require 'db.php';
require 'style.php';
  $redis=new Predis\Client();
require 'db.php';

$sql = 'SELECT * FROM utilisateurs';
$sql1 = 'SELECT * FROM utilisateurs1';

$statement = $connection->prepare($sql);
$statement1 = $connection->prepare($sql1);

$statement->execute();
$statement1->execute();

$people = $statement->fetchAll(PDO::FETCH_OBJ);
$people1 = $statement1->fetchAll(PDO::FETCH_OBJ);


 ?>
<?php require 'header.php'; ?>
<div class="container">
<a style="color: mistyrose;float: left;" href="index.php"><span class="fa fa-caret-left">&ensp;Insertion </span></a><a style="color: mistyrose;float: right;" href="supp.php">supression <span class="fa fa-caret-right"></span></a>

    <div class="row">
        <div class="col-md-6">
            <div class="card mt-5">
                <div class="card-header">
                <h2>Liste Utilisateurs 01&ensp;<span class="fa fa-users"></h2>
                </div>
                <div class="card-body">
                  <h5>Mysql</h5>
      <table class="table table-bordered">
        <tr>
          <th>Nom</th>
          <th>Note</th>
        </tr>
        <?php foreach($people as $person): ?>
          <tr>
            <td><?= $person->nom; ?></td>
            <td><?= $person->note; ?></td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
                </div>
  </div>
  <div class="col-md-6">
            <div class="card mt-5">
                <div class="card-header">
                <h2>Liste Utilisateurs 02&ensp;<span class="fa fa-users"></h2>
                </div>
                <div class="card-body">
                  <h5>Mysql</h5>
      <table class="table table-bordered">
        <tr>
          <th>Nom</th>
          <th>Note</th>
        </tr>
        <?php foreach($people1 as $person1): ?>
          <tr>
            <td><?= $person1->nom; ?></td>
            <td><?= $person1->note; ?></td>
          </tr>
        <?php endforeach; ?>
      </table>
     
    </div>
                </div>
  </div>
</div>
 <div class="row" style="padding-top: 8px;">
     <div class="col-md-1 offset-5" style="padding-left: 60px;"><button class="btn btn-danger"><a onclick="return confirm('Voulez vous vraiment faire une union entre les 2 table?')" href="unions.php" class='btn btn-danger'>Union</a></button></div>
 </div>
</div>
<?php require 'footer.php'; ?>
